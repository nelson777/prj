 requirejs.config({
     baseUrl: 'js/',
     paths: {
         text: '../vendor/require/text',
         json: '../vendor/require/json',
         jquery: '../vendor/jquery/jquery.min',
         moment: '../vendor/moment/moment.min',
         crossroads: '../vendor/crossroads/crossroads.min',
         hasher: '../vendor/hasher/hasher.min',
         signals: '../vendor/signals/signals.min',
         nunjucks: '../vendor/nunjucks/nunjucks.min',
         lodash: '../vendor/lodash/lodash.min'
     }

 });

 require(['main']);
