define(['jquery', 'moment', 'crossroads', 'hasher', 'nunjucks',
        'json!helpers/projectInfo.json', ],
function($, moment, crossroads, hasher, nunjucks,
         pi) {

    $('#root').text(pi.name + ' ' + moment().format('DD/MM/YYYY'));

    nunjucks.configure('js/templates', { autoescape: true });

    var db = nunjucks.render("doublebutton.tpl", pi);

    $(db).insertAfter("#root");

    function about() {
        console.log('about');
    }
    function home() {
        console.log('home');
    }

    crossroads.addRoute('/about', about);
    crossroads.addRoute('/home', home);
    //setup hasher
    function parseHash(newHash, oldHash){
      crossroads.parse(newHash);
    }
    hasher.initialized.add(parseHash); //parse initial hash
    hasher.changed.add(parseHash); //parse hash changes
    hasher.init(); //start listening for history change

 });

/*
<div id="btnTest">
    <div class="ui buttons">
      <button class="ui button">Cancel</button>
      <div class="or"></div>
      <button class="ui positive button">Finish</button>
    </div>
</div>
*/
