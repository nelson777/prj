{% extends "base/header.tpl" %}
{% block btn %}
<div id='btnTest'>
    <div class='ui buttons'>
      <button class='ui button'>{{ btnLeft }}</button>
      <div class='or'></div>
      <button class='ui positive button'>{{ btnRight }}</button>
    </div>
</div>
{% endblock %}
